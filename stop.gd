extends Area2D

const SPEED = 800

@onready var black_hole_sprite = $BlackHole
@onready var stopper_sprite = $Stopper

# Called when the node enters the scene tree for the first time.
func _ready():
	var number = randi_range(0, 1)
	
	if number == 0:
		black_hole_sprite.visible = false
	else:
		stopper_sprite.visible = false


func _process(delta):
	position.y += SPEED*delta

func _on_area_entered(area):
	if area.is_in_group("Player"):
		area.queue_free()

