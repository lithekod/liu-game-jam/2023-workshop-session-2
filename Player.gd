extends Area2D

@export var label: Label
const JUMP_DISTANCE = 560
var score = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if Input.is_action_just_pressed("left"):
		position.x -= JUMP_DISTANCE
		
	elif Input.is_action_just_pressed("right"):
		position.x += JUMP_DISTANCE

	position.x = clamp(position.x, -JUMP_DISTANCE, JUMP_DISTANCE)
	
func add_score():
	score += 1
	label.text = str(score)
	
