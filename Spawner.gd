extends Node2D

const JUMP_DISTANCE = 560
const SPAWN_CONST_DELAY = 0.6
const SPAWN_VAR_DELAY = 0.6
var next_spawn_time = 0.0
var timer = 0.0

@onready var alien_tscn = preload("res://aliens.tscn")
@onready var stopper_tscn = preload("res://stop.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	timer += delta
	
	if timer >= next_spawn_time:
		var spawn_pos = randi_range(-1, 1) * JUMP_DISTANCE
		
		var alien_number = randi_range(0, 2)
		if alien_number == 0:
			# do stopper
			var stopper = stopper_tscn.instantiate()
			stopper.position.x = spawn_pos
			add_child(stopper)
		
		else:
			# alien
			var alien = alien_tscn.instantiate()
			alien.position.x = spawn_pos
			add_child(alien)
		
		next_spawn_time = SPAWN_CONST_DELAY + randf_range(0, SPAWN_VAR_DELAY)
		timer = 0
