extends Area2D

const SPEED = 800

func _process(delta):
	position.y += SPEED*delta

func _on_area_entered(area):
	if area.is_in_group("Player"):
		area.add_score()
		queue_free()
